# test_msig
# SQL create table if need cause already use online db
create table attendance (id bigserial not null, date timestamp(6), present boolean not null, class_id bigint, student_id bigint, primary key (id))
create table classes (id bigserial not null, class_name varchar(255), schedule timestamp(6), teacher_id bigint, primary key (id))
create table student (id bigserial not null, address varchar(255), age integer not null, email varchar(255), gender varchar(255), grade varchar(255), name varchar(255), phone_number varchar(255), primary key (id))
create table teacher (id bigserial not null, department varchar(255), email varchar(255), experience integer not null, hire_date timestamp(6), name varchar(255), phone_number varchar(255), subject varchar(255), primary key (id))
alter table if exists attendance add constraint FKrx58locko31i5sa3goghxssli foreign key (class_id) references classes
alter table if exists attendance add constraint FKnq6vm31it076obtjf2qp5coim foreign key (student_id) references student
alter table if exists classes add constraint FK9vbmf9aq55wlc5ektka70hq1d foreign key (teacher_id) references teacher




