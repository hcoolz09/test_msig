import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import {
  Table,
  Button,
  Modal,
  Form,
  Input,
  DatePicker,
  Select,
  message,
} from "antd";
import moment from "moment";

const { Column } = Table;
const { Option } = Select;

function ClassList() {
  const [classes, setClasses] = useState([]);
  const [teachers, setTeachers] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editingClass, setEditingClass] = useState(null);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchClasses();
    fetchTeachers();
  }, []);

  const fetchClasses = () => {
    axiosInstance
      .get("/classes")
      .then((response) => {
        setClasses(response.data);
      })
      .catch((error) => {
        console.error("There was an error fetching the classes!", error);
      });
  };

  const fetchTeachers = () => {
    axiosInstance
      .get("/teachers")
      .then((response) => {
        setTeachers(response.data);
      })
      .catch((error) => {
        console.error("There was an error fetching the teachers!", error);
      });
  };

  const handleDelete = (id) => {
    axiosInstance
      .delete(`/classes/${id}`)
      .then(() => {
        message.success("Class deleted successfully");
        fetchClasses();
      })
      .catch((error) => {
        console.error("There was an error deleting the class!", error);
      });
  };

  const handleEdit = (classItem) => {
    const formattedClassItem = {
      ...classItem,
      schedule: moment(classItem.schedule),
      teacher: classItem.teacher.name, 
    };
    setEditingClass(formattedClassItem);
    form.setFieldsValue(formattedClassItem); 
    setIsModalVisible(true);
  };

  const handleModalCancel = () => {
    form.resetFields();
    setIsModalVisible(false);
    setEditingClass(null);
  };

  const handleFormSubmit = (values) => {
    const classData = {
      ...values,
      schedule: values.schedule
        ? values.schedule.format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
        : null,
    };

    if (editingClass) {
      axiosInstance
        .put(`/classes/${editingClass.id}`, classData)
        .then(() => {
          message.success("Class updated successfully");
          fetchClasses();
        })
        .catch((error) => {
          console.error("There was an error updating the class!", error);
        });
    } else {
      axiosInstance
        .post("/classes", classData)
        .then(() => {
          message.success("Class added successfully");
          fetchClasses();
        })
        .catch((error) => {
          console.error("There was an error adding the class!", error);
        });
    }
    handleModalCancel();
  };

  useEffect(() => {
  if (!isModalVisible) {
    form.resetFields();
    setEditingClass(null);
  }
}, [isModalVisible, form]);

  return (
    <div>
      <h2>Classes</h2>
      <Button
        type="primary"
        onClick={() => {
          form.resetFields();
          setEditingClass(null);
          setIsModalVisible(true);
        }}
      >
        Add Class
      </Button>
      <Table dataSource={classes} rowKey="id">
        <Column title="Class Name" dataIndex="className" key="className" />
        <Column
          title="Schedule"
          dataIndex="schedule"
          key="schedule"
          render={(text) => (text ? moment(text).format("YYYY-MM-DD") : "")}
        />
        <Column
          title="Teacher"
          key="teacher"
          render={(text, record) => record.teacher?.name}
        />
        <Column title="Duration" dataIndex="duration" key="duration" />
        <Column
          title="Action"
          key="action"
          render={(text, record) => (
            <>
              <Button
                onClick={() => handleEdit(record)}
                style={{ marginRight: 8 }}
              >
                Edit
              </Button>
              <Button onClick={() => handleDelete(record.id)} danger>
                Delete
              </Button>
            </>
          )}
        />
      </Table>
      <Modal
        title={editingClass ? "Edit Class" : "Add Class"}
        open={isModalVisible}
        onCancel={handleModalCancel}
        footer={null}
      >
        <Form
          form={form}
          initialValues={editingClass}
          onFinish={handleFormSubmit}
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item
            name="className"
            label="Class Name"
            rules={[
              { required: true, message: "Please input the class name!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="schedule"
            label="Schedule"
            rules={[{ required: true, message: "Please select the schedule!" }]}
          >
            <DatePicker
              onChange={(date) => form.setFieldsValue({ schedule: date })}
              format="YYYY-MM-DD"
              value={form.getFieldValue("schedule")}
            />
          </Form.Item>
          <Form.Item
            name="teacher"
            label="Teacher"
            rules={[{ required: true, message: "Please select a teacher!" }]}
          >
            <Select>
              {teachers.map((teacher) => (
                <Option key={teacher.id} value={teacher.id}>
                  {teacher.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name="duration"
            label="Duration"
            rules={[{ required: true, message: "Please input the duration!" }]}
          >
            <Input type="number" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              {editingClass ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default ClassList;
