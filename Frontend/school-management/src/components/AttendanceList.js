import React, { useState, useEffect } from 'react';
import axiosInstance from '../axiosConfig';
import { Table, Button, Modal, Form, DatePicker, Select, Radio, message } from 'antd';
import moment from 'moment';

const { Column } = Table;
const { Option } = Select;

function AttendanceList() {
  const [attendances, setAttendances] = useState([]);
  const [students, setStudents] = useState([]);
  const [classes, setClasses] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editingAttendance, setEditingAttendance] = useState(null);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchAttendances();
    fetchStudents();
    fetchClasses();
  }, []);

  const fetchAttendances = () => {
    axiosInstance.get('/attendance')
      .then(response => {
        setAttendances(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the attendance records!", error);
      });
  };

  const fetchStudents = () => {
    axiosInstance.get('/students')
      .then(response => {
        setStudents(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the students!", error);
      });
  };

  const fetchClasses = () => {
    axiosInstance.get('/classes')
      .then(response => {
        setClasses(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the classes!", error);
      });
  };

  const handleDelete = (id) => {
    axiosInstance.delete(`/attendance/${id}`)
      .then(() => {
        message.success('Attendance record deleted successfully');
        fetchAttendances();
      })
      .catch(error => {
        console.error("There was an error deleting the attendance record!", error);
      });
  };

  const handleEdit = (attendance) => {
    setEditingAttendance({
      ...attendance,
      date: moment(attendance.date),
      student: attendance.student.id,
      class: attendance.classEntity.id,
    });
    setIsModalVisible(true);
  };

  const handleModalCancel = () => {
    setIsModalVisible(false);
    setEditingAttendance(null);
    form.resetFields();
  };

  const handleFormSubmit = (values) => {
    const formattedValues = {
      ...values,
      date: values.date ? values.date.format('YYYY-MM-DDTHH:mm:ss.SSS[Z]') : null,
    };

    if (editingAttendance) {
      axiosInstance.put(`/attendance/${editingAttendance.id}`, formattedValues)
        .then(() => {
          message.success('Attendance record updated successfully');
          fetchAttendances();
        })
        .catch(error => {
          console.error("There was an error updating the attendance record!", error);
        });
    } else {
      axiosInstance.post('/attendance', formattedValues)
        .then(() => {
          message.success('Attendance record added successfully');
          fetchAttendances();
        })
        .catch(error => {
          console.error("There was an error adding the attendance record!", error);
        });
    }
    handleModalCancel();
  };

  useEffect(() => {
    if (isModalVisible) {
      if (editingAttendance) {
        form.setFieldsValue(editingAttendance);
      } else {
        form.resetFields();
      }
    }
  }, [isModalVisible, editingAttendance, form]);

  return (
    <div>
      <h2>Attendance Records</h2>
      <Button type="primary" onClick={() => setIsModalVisible(true)}>Add Attendance</Button>
      <Table dataSource={attendances} rowKey="id">
        <Column
          title="Date"
          dataIndex="date"
          key="date"
          render={(text) => (text ? moment(text).format("YYYY-MM-DD") : "")}
        />
        <Column title="Student" dataIndex={['student', 'name']} key="student" />
        <Column title="Present" dataIndex="present" key="present" render={present => present ? 'Present' : 'Absent'} />
        <Column title="Class" dataIndex={['classEntity', 'className']} key="class" />
        <Column
          title="Action"
          key="action"
          render={(text, record) => (
            <>
              <Button onClick={() => handleEdit(record)} style={{ marginRight: 8 }}>Edit</Button>
              <Button onClick={() => handleDelete(record.id)} danger>Delete</Button>
            </>
          )}
        />
      </Table>
      <Modal
        title={editingAttendance ? "Edit Attendance" : "Add Attendance"}
        visible={isModalVisible}
        onCancel={handleModalCancel}
        footer={null}
      >
        <Form
          form={form}
          onFinish={handleFormSubmit}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item name="date" label="Date" rules={[{ required: true, message: 'Please input the date!' }]}>
            <DatePicker format="YYYY-MM-DD" />
          </Form.Item>
          <Form.Item name="student" label="Student" rules={[{ required: true, message: 'Please select a student!' }]}>
            <Select>
              {students.map(student => (
                <Option key={student.id} value={student.id}>{student.name}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name="present" label="Present" rules={[{ required: true, message: 'Please select the attendance status!' }]}>
            <Radio.Group>
              <Radio value={true}>Present</Radio>
              <Radio value={false}>Absent</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item name="class" label="Class" rules={[{ required: true, message: 'Please select a class!' }]}>
            <Select>
              {classes.map(classItem => (
                <Option key={classItem.id} value={classItem.id}>{classItem.className}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              {editingAttendance ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default AttendanceList;
