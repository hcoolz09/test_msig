import React, { useState, useEffect } from 'react';
import axiosInstance from '../axiosConfig';
import { Table, Button, Modal, Form, Input, message, Radio, Select } from 'antd';

const { Column } = Table;
const { Option } = Select;

function StudentList() {
  const [students, setStudents] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editingStudent, setEditingStudent] = useState(null);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchStudents();
  }, []);

  const fetchStudents = () => {
    axiosInstance.get('/students')
      .then(response => {
        setStudents(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the students!", error);
      });
  };

  const handleDelete = (id) => {
    axiosInstance.delete(`/students/${id}`)
      .then(() => {
        message.success('Student deleted successfully');
        fetchStudents();
      })
      .catch(error => {
        console.error("There was an error deleting the student!", error);
      });
  };

  const handleEdit = (student) => {
    setEditingStudent(student);
    form.setFieldsValue(student);
    setIsModalVisible(true);
  };

  const handleModalCancel = () => {
    setIsModalVisible(false);
    setEditingStudent(null);
    form.resetFields();
  };

  const handleFormSubmit = (values) => {
    if (editingStudent) {
      axiosInstance.put(`/students/${editingStudent.id}`, values)
        .then(() => {
          message.success('Student updated successfully');
          fetchStudents();
        })
        .catch(error => {
          console.error("There was an error updating the student!", error);
        });
    } else {
      axiosInstance.post('/students', values)
        .then(() => {
          message.success('Student added successfully');
          fetchStudents();
        })
        .catch(error => {
          console.error("There was an error adding the student!", error);
        });
    }
    handleModalCancel();
  };

  return (
    <div>
      <h2>Students</h2>
      <Button type="primary" onClick={() => {
        form.resetFields();
        setEditingStudent(null);
        setIsModalVisible(true);
      }}>Add Student</Button>
      <Table dataSource={students} rowKey="id">
        <Column title="Name" dataIndex="name" key="name" />
        <Column title="Email" dataIndex="email" key="email" />
        <Column title="Age" dataIndex="age" key="age" />
        <Column title="Address" dataIndex="address" key="address" />
        <Column title="Phone" dataIndex="phoneNumber" key="phoneNumber" />
        <Column title="Gender" dataIndex="gender" key="gender" />
        <Column title="Grade" dataIndex="grade" key="grade" />
        <Column
          title="Action"
          key="action"
          render={(text, record) => (
            <>
              <Button onClick={() => handleEdit(record)} style={{ marginRight: 8 }}>Edit</Button>
              <Button onClick={() => handleDelete(record.id)} danger>Delete</Button>
            </>
          )}
        />
      </Table>
      <Modal
        title={editingStudent ? "Edit Student" : "Add Student"}
        visible={isModalVisible}
        onCancel={handleModalCancel}
        footer={null}
      >
        <Form
          form={form}
          onFinish={handleFormSubmit}
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item name="name" label="Name" rules={[{ required: true, message: 'Please input the name!' }]}>
            <Input />
          </Form.Item>
          <Form.Item name="email" label="Email" rules={[{ required: true, message: 'Please input the email!' }]}>
            <Input />
          </Form.Item>
          <Form.Item name="age" label="Age" rules={[{ required: true, message: 'Please input the age!' }]}>
            <Input type="number"/>
          </Form.Item>
          <Form.Item name="address" label="Address" rules={[{ required: true, message: 'Please input the address!' }]}>
            <Input />
          </Form.Item>
          <Form.Item name="phoneNumber" label="Phone" rules={[{ required: true, message: 'Please input the phone!' }]}>
            <Input type="number"/>
          </Form.Item>
          <Form.Item name="gender" label="Gender" rules={[{ required: true, message: 'Please select the gender!' }]}>
            <Radio.Group>
              <Radio value="Male">Male</Radio>
              <Radio value="Female">Female</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item name="grade" label="Grade" rules={[{ required: true, message: 'Please select the grade!' }]}>
            <Select>
              <Option value="1th">1th</Option>
              <Option value="2th">2th</Option>
              <Option value="3th">3th</Option>
              <Option value="4th">4th</Option>
              <Option value="5th">5th</Option>
              <Option value="6th">6th</Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              {editingStudent ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default StudentList;
