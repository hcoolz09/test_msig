import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import {
  Table,
  Button,
  Modal,
  Form,
  Input,
  message,
  DatePicker
} from "antd";
import moment from 'moment';

const { Column } = Table;

function TeacherList() {
  const [teachers, setTeachers] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editingTeacher, setEditingTeacher] = useState(null);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchTeachers();
  }, []);

  const fetchTeachers = () => {
    axiosInstance
      .get("/teachers")
      .then((response) => {
        setTeachers(response.data);
      })
      .catch((error) => {
        console.error("There was an error fetching the teachers!", error);
      });
  };

  const handleDelete = (id) => {
    axiosInstance
      .delete(`/teachers/${id}`)
      .then(() => {
        message.success("Teacher deleted successfully");
        fetchTeachers();
      })
      .catch((error) => {
        console.error("There was an error deleting the teacher!", error);
      });
  };

  const handleEdit = (teacher) => {
    setEditingTeacher({
      ...teacher,
      hireDate: teacher.hireDate ? moment(teacher.hireDate) : null
    });
    setIsModalVisible(true);
  };

  const handleModalCancel = () => {
    setIsModalVisible(false);
    setEditingTeacher(null);
    form.resetFields();
  };

  const handleFormSubmit = (values) => {
    const formattedValues = {
      ...values,
      hireDate: values.hireDate ? values.hireDate.format('YYYY-MM-DDTHH:mm:ss.SSS[Z]') : null,
    };

    if (editingTeacher) {
      axiosInstance
        .put(`/teachers/${editingTeacher.id}`, formattedValues)
        .then(() => {
          message.success("Teacher updated successfully");
          fetchTeachers();
        })
        .catch((error) => {
          console.error("There was an error updating the teacher!", error);
        });
    } else {
      axiosInstance
        .post("/teachers", formattedValues)
        .then(() => {
          message.success("Teacher added successfully");
          fetchTeachers();
        })
        .catch((error) => {
          console.error("There was an error adding the teacher!", error);
        });
    }
    handleModalCancel();
  };

  useEffect(() => {
    if (isModalVisible && editingTeacher) {
      form.setFieldsValue(editingTeacher);
    }
  }, [isModalVisible, editingTeacher, form]);

  return (
    <div>
      <h2>Teachers</h2>
      <Button type="primary" onClick={() => {
        form.resetFields();
        setEditingTeacher(null);
        setIsModalVisible(true);
      }}>
        Add Teacher
      </Button>
      <Table dataSource={teachers} rowKey="id">
        <Column title="Name" dataIndex="name" key="name" />
        <Column title="Email" dataIndex="email" key="email" />
        <Column title="Subject" dataIndex="subject" key="subject" />
        <Column title="Phone" dataIndex="phoneNumber" key="phoneNumber" />
        <Column title="Department" dataIndex="department" key="department" />
        <Column title="Experience" dataIndex="experience" key="experience" />
        <Column title="HireDate" dataIndex="hireDate" key="hireDate" render={(text) => (text ? moment(text).format('YYYY-MM-DD') : '')} />
        <Column
          title="Action"
          key="action"
          render={(text, record) => (
            <>
              <Button
                onClick={() => handleEdit(record)}
                style={{ marginRight: 8 }}
              >
                Edit
              </Button>
              <Button onClick={() => handleDelete(record.id)} danger>
                Delete
              </Button>
            </>
          )}
        />
      </Table>
      <Modal
        title={editingTeacher ? "Edit Teacher" : "Add Teacher"}
        visible={isModalVisible}
        onCancel={handleModalCancel}
        footer={null}
        afterClose={() => form.resetFields()}
      >
        <Form
          form={form}
          onFinish={handleFormSubmit}
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item
            name="name"
            label="Name"
            rules={[{ required: true, message: "Please input the name!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="email"
            label="Email"
            rules={[{ required: true, message: "Please input the email!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="subject"
            label="Subject"
            rules={[{ required: true, message: "Please input the subject!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            label="Phone"
            rules={[{ required: true, message: "Please input the phone!" }]}
          >
            <Input type="number" />
          </Form.Item>
          <Form.Item
            name="department"
            label="Department"
            rules={[
              { required: true, message: "Please input the department!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="hireDate"
            label="Hire Date"
            rules={[{ required: true, message: "Please select the date!" }]}
          >
            <DatePicker
              onChange={(date) => form.setFieldsValue({ hireDate: date })}
              format="YYYY-MM-DD"
              value={form.getFieldValue('hireDate')}
            />
          </Form.Item>
          <Form.Item
            name="experience"
            label="Experience"
            rules={[{ required: true, message: "Please input the experience!" }]}
          >
            <Input type="number" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              {editingTeacher ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default TeacherList;
