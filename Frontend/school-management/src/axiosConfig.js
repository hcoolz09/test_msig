import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'http://localhost:8080', 
  timeout: 10000, 
  headers: {
    'Content-Type': 'application/json',
    // 'Access-Control-Allow-Origin': '*',
    // 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
    // 'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept',
  },
});

export default axiosInstance;