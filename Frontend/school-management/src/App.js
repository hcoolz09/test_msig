import React, { useState } from 'react';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  BookOutlined,
  CheckCircleOutlined
} from '@ant-design/icons';
import StudentList from './components/StudentList';
import TeacherList from './components/TeacherList';
import ClassList from './components/ClassList';
import AttendanceList from './components/AttendanceList';

const { Header, Content, Sider } = Layout;

function App() {
  const [current, setCurrent] = useState('students');

  const handleClick = e => {
    setCurrent(e.key);
  };

  const renderComponent = () => {
    switch (current) {
      case 'students':
        return <StudentList />;
      case 'teachers':
        return <TeacherList />;
      case 'classes':
        return <ClassList />;
      case 'attendance':
        return <AttendanceList />;
      default:
        return <StudentList />;
    }
  };

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible>
        <Menu theme="dark" mode="inline" onClick={handleClick} defaultSelectedKeys={['students']}>
          <Menu.Item key="students" icon={<UserOutlined />}>
            Students
          </Menu.Item>
          <Menu.Item key="teachers" icon={<UserOutlined />}>
            Teachers
          </Menu.Item>
          <Menu.Item key="classes" icon={<BookOutlined />}>
            Classes
          </Menu.Item>
          <Menu.Item key="attendance" icon={<CheckCircleOutlined />}>
            Attendance
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }} />
        <Content style={{ margin: '0 16px' }}>
          <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
            {renderComponent()}
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}

export default App;
