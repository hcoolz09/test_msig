package com.example.school.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.school.configuration.DateUtil;
import com.example.school.model.Teacher;
import com.example.school.repository.TeacherRepo;

import jakarta.transaction.Transactional;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepo teacherRepository;

    @Transactional
    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }

    @Transactional
    public Optional<Teacher> getTeacherById(Long id) {
        return teacherRepository.findById(id);
    }

    @Transactional
    public Teacher addTeacher(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    @Transactional
    public void deleteTeacher(Long id) {
        teacherRepository.deleteById(id);
    }

    @Transactional
    public void editTeacher(Long id, Map<String,Object> updatedTeacher) {
        Optional<Teacher> optionalTeacher = teacherRepository.findById(id);
        if (optionalTeacher.isPresent()) {
            Teacher existingTeacber = optionalTeacher.get();
            existingTeacber.setName(updatedTeacher.get("name").toString());
            existingTeacber.setEmail(updatedTeacher.get("email").toString());
            existingTeacber.setSubject(updatedTeacher.get("subject").toString());
            existingTeacber.setExperience(Integer.parseInt(updatedTeacher.get("experience").toString()));
            existingTeacber.setDepartment(updatedTeacher.get("department").toString());
            existingTeacber.setPhoneNumber(updatedTeacher.get("phoneNumber").toString());
            existingTeacber.setHireDate(DateUtil.parseStringToLocalDateTime(updatedTeacher.get("hireDate").toString()));
            teacherRepository.save(existingTeacber);
        } else {
            throw new RuntimeException("Teacher not found with id: " + id);
        }
    }
}

