package com.example.school.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.school.configuration.DateUtil;
import com.example.school.model.Classes;
import com.example.school.model.Teacher;
import com.example.school.repository.ClassRepo;
import com.example.school.repository.TeacherRepo;

import jakarta.transaction.Transactional;

@Service
public class ClassService {
    @Autowired
    private ClassRepo classRepository;

    @Autowired
    private TeacherRepo teacherRepository;

    @Transactional
    public List<Classes> getAllClasses() {
        return classRepository.findAll();
    }

    @Transactional
    public Optional<Classes> getClassById(Long id) {
        return classRepository.findById(id);
    }

    @Transactional
    public void addClass(Map<String,Object> classEntity) {
        Classes classes = new Classes();
        Teacher teacher = teacherRepository.findById(Long.valueOf(classEntity.get("teacher").toString())).orElse(null);
        classes.setClassName(classEntity.get("className").toString());
        classes.setSchedule(DateUtil.parseStringToLocalDateTime(classEntity.get("schedule").toString()));
        classes.setTeacher(teacher);
        classes.setDuration(Integer.parseInt(classEntity.get("duration").toString()));

        classRepository.save(classes);
    }

    @Transactional
    public void deleteClass(Long id) {
        classRepository.deleteById(id);
    }

    @Transactional
    public void editClass(Long id, Map<String,Object> updatedClass) {
        Optional<Classes> optionalClass = classRepository.findById(id);
        if (optionalClass.isPresent()) {
            Classes existingClass = optionalClass.get();
            Teacher teacher = teacherRepository.findById(Long.valueOf(updatedClass.get("teacher").toString())).orElse(null);
            existingClass.setClassName(updatedClass.get("className").toString());
            existingClass.setSchedule(DateUtil.parseStringToLocalDateTime(updatedClass.get("schedule").toString()));
            existingClass.setTeacher(teacher);
            existingClass.setDuration(Integer.parseInt(updatedClass.get("duration").toString()));
            classRepository.save(existingClass);
        } else {
            throw new RuntimeException("Class not found with id: " + id);
        }
    }
}
