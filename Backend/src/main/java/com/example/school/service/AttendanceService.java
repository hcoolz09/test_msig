package com.example.school.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.school.configuration.DateUtil;
import com.example.school.model.Attendance;
import com.example.school.model.Classes;
import com.example.school.model.Student;
import com.example.school.repository.AttendanceRepo;
import com.example.school.repository.ClassRepo;
import com.example.school.repository.StudentRepo;

import jakarta.transaction.Transactional;

@Service
public class AttendanceService {
    @Autowired
    private AttendanceRepo attendanceRepository;

    @Autowired
    private StudentRepo studentRepo;

    @Autowired
    private ClassRepo classRepo;

    @Transactional
    public List<Attendance> getAllAttendanceRecords() {
        return attendanceRepository.findAll();
    }

    @Transactional
    public Optional<Attendance> getAttendanceRecordById(Long id) {
        return attendanceRepository.findById(id);
    }

    @Transactional
    public Attendance addAttendanceRecord(Map<String,Object> attendance) {
        Attendance attendanceSave = new Attendance();
        Student student = studentRepo.findById(Long.valueOf(attendance.get("student").toString())).orElse(null);
        Classes classes = classRepo.findById(Long.valueOf(attendance.get("class").toString())).orElse(null);
        attendanceSave.setStudent(student);
        attendanceSave.setClassEntity(classes);
        attendanceSave.setDate(DateUtil.parseStringToLocalDateTime(attendance.get("date").toString()));
        attendanceSave.setPresent(Boolean.parseBoolean(attendance.get("present").toString()));

        return attendanceRepository.save(attendanceSave);
    }

    @Transactional
    public void deleteAttendanceRecord(Long id) {
        attendanceRepository.deleteById(id);
    }

    @Transactional
    public void editAttendance(Long id, Map<String,Object> updatedAttendance) {
        Optional<Attendance> optionalAttendance = attendanceRepository.findById(id);
        if (optionalAttendance.isPresent()) {
            Student student = studentRepo.findById(Long.valueOf(updatedAttendance.get("student").toString())).orElse(null);
            Classes classes = classRepo.findById(Long.valueOf(updatedAttendance.get("class").toString())).orElse(null);
            Attendance existingAttendance = optionalAttendance.get();
            existingAttendance.setStudent(student);
            existingAttendance.setClassEntity(classes);
            existingAttendance.setDate(DateUtil.parseStringToLocalDateTime(updatedAttendance.get("date").toString()));
            existingAttendance.setPresent(Boolean.parseBoolean(updatedAttendance.get("present").toString()));
            attendanceRepository.save(existingAttendance);
        } else {
            throw new RuntimeException("Attendance not found with id: " + id);
        }
    }
}
