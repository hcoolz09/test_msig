package com.example.school.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.school.model.Attendance;
import com.example.school.service.AttendanceService;

@RestController
@RequestMapping("/attendance")
public class AttendanceController {
    @Autowired
    private AttendanceService attendanceService;

    @GetMapping
    public List<Attendance> getAllAttendanceRecords() {
        return attendanceService.getAllAttendanceRecords();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Attendance> getAttendanceRecordById(@PathVariable Long id) {
        return attendanceService.getAttendanceRecordById(id)
            .map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public void addAttendanceRecord(@RequestBody Map<String,Object> attendance) {
        attendanceService.addAttendanceRecord(attendance);
    }

    @DeleteMapping("/{id}")
    public void deleteAttendanceRecord(@PathVariable Long id) {
        attendanceService.deleteAttendanceRecord(id);
    }
    
    @PutMapping("/{id}")
    public void editAttendance(@PathVariable Long id,@RequestBody Map<String,Object> updatedAttendance) {
        attendanceService.editAttendance(id, updatedAttendance);
    }
}
