package com.example.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.school.model.Classes;

public interface ClassRepo extends JpaRepository<Classes, Long> {

}
