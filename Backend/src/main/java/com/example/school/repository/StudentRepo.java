package com.example.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.school.model.Student;

public interface StudentRepo extends JpaRepository<Student, Long> {
    
}
