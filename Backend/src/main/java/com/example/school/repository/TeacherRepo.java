package com.example.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.school.model.Teacher;

public interface TeacherRepo extends JpaRepository<Teacher, Long> {

}
